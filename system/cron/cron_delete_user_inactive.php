<?php

$base = dirname(dirname(__FILE__));
include_once($base . '/cron/functions_sql/functions_sql.php');
include_once($base . '/cron/base_path.php');

$dir_upload = PATH_DOMAIN . '/src/img/company/logo/';

$type_user = array(
	'person' => array(
		'table' => 'person',
		'prefix' => 'pi',
		'col_ofert_apply' => 'tbl_person_info_id_pi',
		'fields' => 'id_pi',
	), 
	'company' => array(
		'table' => 'company',
		'prefix' => 'ci',
		'fields' => 'id_ci, image_ci',
	),
);

$time = '2 DAY';

foreach ($type_user as $key_type => $value) {
	$users_inactived = model_user_get_user_inactive($value['table'], $value['prefix'], $value['fields'], $time);

  if (empty($users_inactived)) {
    echo 'No hay usuarios ' . $value['table'] . ' que eliminar<br>';

  }else {
  	foreach ($users_inactived as $key_user => $value_user) {

  		if ($value['prefix'] == 'ci') {
				if (!unlink($dir_upload . $value_user['image_' . $value['prefix']])) {
          rename($dir_upload . $value_user['image_' . $value['prefix']], $dir_upload . 'REMOVE_' . $value_user['image_' . $value['prefix']]);
        } 
  		}

			$delete_user = model_user_delete_user($value['table'], $value['prefix'], $value_user['id_' . $value['prefix']]);

			if($delete_user == 1) {
				echo '<br>usuario eliminado ok<br>';
			}else {
				echo '<br>error al eliminar usuario<br>';
			}
		}
  }
}

function model_user_get_user_inactive($tbl, $prefix, $fields, $time) {

	$sql = 'SELECT ' . $fields . ' FROM    tbl_' . $tbl . '_info
					WHERE active_' . $prefix . ' = 0 AND 
					created_' . $prefix . ' <= NOW() - INTERVAL ' . $time . ';';

	$result = functions_sql_execute_query($sql);
  if (!empty($result)) {
    $rows = array();
    while($item = functions_sql_execute_get_dates($result)) {
      $rows[] = $item;
    }
    functions_sql_close_query_and_connection($result);
    return $rows;
  }
  functions_sql_close_query_and_connection($result);
  return FALSE;
}


function model_user_delete_user($tbl, $prefix, $id_user) {
	$sql = 'DELETE FROM tbl_' . $tbl . '_info WHERE id_' . $prefix . '=' . $id_user .';';
	$response = functions_sql_execute_query($sql);
	functions_sql_close_connection();

	return $response;
}