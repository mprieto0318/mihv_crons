<?php

$base = dirname(dirname(__FILE__));
include_once($base . '/cron/functions_sql/functions_sql.php');
include_once($base . '/cron/base_path.php');

$companys = get_all_company();

if (empty($companys)) {
  echo 'No hay empresas<br>';

}else {
	$data_email = array();

	foreach ($companys as $key_company => &$value_company) {

		// 			$value_company['id_ci']
		// 			$value_company['name_ci']
		// 			$value_company['email_ci']
		//echo 'key_company: ' . $key_company . ' - value_company: ' . $value_company['name_ci'] . '<br>';

		$data_email['name_ci'] = $value_company['name_ci'];
		$data_email['email_ci'] = $value_company['email_ci'];

		$offers = get_all_offers($value_company['id_ci']);

		if (!empty($offers)) {
			foreach ($offers as $key_offer => $value_offer) {

				// 			$value_offer['id_o']
				// 			$value_offer['name_o']
				//echo ' -- key_offer: ' . $key_offer . ' - value_offer: ' . $value_offer['name_o'] . '<br>';

				$data_email['offers'][$key_offer]['id_o'] = $value_offer['id_o'];
				$data_email['offers'][$key_offer]['name_o'] = $value_offer['name_o'];

				$apply_not_send = get_all_offers_apply($value_offer['id_o'], 0);

				foreach ($apply_not_send as $key_apply_not_send => $value_apply_not_send) {
					//echo ' -- -- key_apply_not_send: ' . $key_apply_not_send . ' - value_apply_not_send: ' . $value_apply_not_send['COUNT(id_oa)'] . '<br>';

					$data_email['offers'][$key_offer]['not_send'] = $value_apply_not_send['COUNT(id_oa)'];
				}

				$apply_send = get_all_offers_apply($value_offer['id_o'], 1);

				foreach ($apply_send as $key_apply_send => $value_apply_send) {
					//echo ' -- -- key_apply_send: ' . $key_apply_send . ' - value_apply_send: ' . $value_apply_send['COUNT(id_oa)'] . '<br>';
					$data_email['offers'][$key_offer]['send'] = $value_apply_send['COUNT(id_oa)'];
				}
	  	}
		
			$email_send = FALSE;
			$update_state = TRUE;
			$list_offers = '<div>';

			foreach ($data_email['offers'] as $key => $value) {
				if($value['not_send'] > 0) {
					$email_send = TRUE;
					$update_state = TRUE;

					$total_apply = $value['not_send'] + $value['send'];
					$list_offers .= '<b> - Oferta: </b><a href="' . DOMAIN . '/views/profile_company.php?tab=2&name-offer=' . $value['name_o'] . '">' . $value['name_o'] . '</a><br>';
					$list_offers .= '<b> - Nuevos Aplicados: </b>' . $value['not_send'] . '<br>';
					$list_offers .= '<b> - Total Aplicados: </b>' . $total_apply . '<br><hr>';

					$response_update = update_state_email_send($value['id_o']);
					
					if (!$response_update) {
						$update_state = FALSE;
						echo 'ha ocurrido un error al actualizar campo email_send = 1 de tbl_oferts_apply <br>';
					}
				}
			}

			$list_offers .= '</div>';

			if ($email_send && $update_state) {
		  	$response_email = send_mail_offer_apply($data_email, $list_offers);

			  if ($response_email) {
			  	echo 'Se envio el correo de nuevos aplicados a ' . $data_email['email_ci'] . '<br>';
			  
			  }else {
			  	echo 'no se pudo enviar el correo de nuevos aplicados a ' . $data_email['email_ci'] . '<br>';
			  }
			}else {
				echo 'No entro al if "$email_send && $update_state", no se envia email ni se actualiza el estado <br>';
			}

		}else {
			echo 'No hay ofertas creadas para la empresa: ' . $data_email['name_ci'];
		}
		
	}
}

function get_all_company() {
	$sql = 'SELECT id_ci, name_ci, email_ci FROM tbl_company_info;';
	return execute_sql_get($sql);
}

function get_all_offers($id_ci) {
	$sql = 'SELECT id_o, name_o FROM tbl_oferts WHERE tbl_company_info_id_ci = ' . $id_ci . ';';
	return execute_sql_get($sql);
}

function get_all_offers_apply($id_o, $send) {
	$sql = 'SELECT COUNT(id_oa) FROM tbl_oferts_apply WHERE tbl_oferts_id_o = ' . $id_o . ' AND email_send = ' . $send . ';';
	return execute_sql_get($sql);
}

function update_state_email_send($id_o) {  
  $sql = 'UPDATE tbl_oferts_apply SET email_send = 1 WHERE tbl_oferts_id_o = ' . $id_o . ';';
    
  $result = functions_sql_execute_query($sql);
  functions_sql_close_connection();

  return $result;
}

function execute_sql_get($sql) {
	$result = functions_sql_execute_query($sql);
  if (!empty($result)) {
  	$rows = array();
    while($item = functions_sql_execute_get_dates($result)) {
    	$rows[] = $item;
    }
    functions_sql_close_query_and_connection($result);
    return $rows;
  }
  functions_sql_close_query_and_connection($result);
  return FALSE;
}

/*
 * Envia correo de notificacion de la cantidad de aplicados en el dia de anterior
 */
function send_mail_offer_apply($data_email, $list_offers) {

  $html = '';
  $html .='

  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
      <meta charset="utf-8"> <!-- utf-8 works for most cases -->
      <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldnt be necessary -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
      <title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->

      <style>

          /* What it does: Remove spaces around the email design added by some email clients. */
          /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
          html,
          body {
              margin: 0 auto !important;
              padding: 0 !important;
              height: 100% !important;
              width: 100% !important;
          }
          
          /* What it does: Stops email clients resizing small text. */
          * {
              -ms-text-size-adjust: 100%;
              -webkit-text-size-adjust: 100%;
          }
          
          /* What is does: Centers email on Android 4.4 */
          div[style*="margin: 16px 0"] {
              margin:0 !important;
          }
          
          /* What it does: Stops Outlook from adding extra spacing to tables. */
          table,
          td {
              mso-table-lspace: 0pt !important;
              mso-table-rspace: 0pt !important;
          }
                  
          /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
          table {
              border-spacing: 0 !important;
              border-collapse: collapse !important;
              table-layout: fixed !important;
              margin: 0 auto !important;
          }
          table table table {
              table-layout: auto; 
          }
          
          /* What it does: Uses a better rendering method when resizing images in IE. */
          img {
              -ms-interpolation-mode:bicubic;
          }
          
          /* What it does: A work-around for iOS meddling in triggered links. */
          .mobile-link--footer a,
          a[x-apple-data-detectors] {
              color:inherit !important;
              text-decoration: none !important;
          }
        
      </style>
      
      <!-- Progressive Enhancements -->
      <style>
          
          /* What it does: Hover styles for buttons */
          .button-td,
          .button-a {
              transition: all 100ms ease-in;
          }
          .button-td:hover,
          .button-a:hover {
              background: #555555 !important;
              border-color: #555555 !important;
          }


      </style>

  </head>
  <body width="100%" bgcolor="#ffffff" style="margin: 0; background: #ffffff;">
      <center style="width: 100%; background: #ffffff;">

          <!-- Visually Hidden Preheader Text : BEGIN -->
          <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
              Hola <b>' . $data_email['name_ci'] . '</b>, este es el resumen diario de tus ofertas.</b>
          </div>
          <!-- Visually Hidden Preheader Text : END -->

          <!--    
              Set the email width. Defined in two places:
              1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 600px.
              2. MSO tags for Desktop Windows Outlook enforce a 600px width.
          -->
          <div style="max-width: 600px; margin: auto;">
              <!--[if mso]>
              <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="600" align="center">
              <tr>
              <td>
              <![endif]-->

              <!-- Email Header : BEGIN -->
              <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">
                  <tr>
                      <td style="padding: 20px 0; text-align: center;">
                          <img alt="Header mi hv" src="https://imagenes.mihv.com.co/email/header-logo.jpg" width="90%" height="50" alt="alt_text" border="0" style="background: none; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                      </td>
                  </tr>
              </table>
              <!-- Email Header : END -->
              
              <!-- Email Body : BEGIN -->
              <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">
                  
                  <!-- 1 Column Text + Button : BEGIN -->
                  <tr>
                      <td bgcolor="#ffffff">
                          <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                              <tr>
                                  <td style="padding: 40px; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                      Hola <b>' . $data_email['name_ci'] . '</b>
                                      <br><br>
                                      Este es el resumen de tus ofertas creadas
                                      <br><br>
                                      <center><b>LISTADO DE OFERTAS</b></center>
                                      <hr>
                                      
                                      ' . $list_offers . '

                                      <br><b>"¡Debes tener tu sesión inicia en ' . DOMAIN . ' para hacer uso de los enlaces!"</b><br><br>
                                      
                                      <br><br>

                                      Éxitos en tu búsqueda y felíz resto de día.
                                      <br><br><br>
                                      El equipo de Mi HV.
                                      <br><br><br>
                                      PD: Posiblemente te interese...<br>
                                  </td>
                                  </tr>
                          </table>
                      </td>
                  </tr>
              </table>
              <!-- Email Body : END -->

              <!-- 2 Even Columns : BEGIN -->
                  <tr>                    
                      <td bgcolor="#ffffff" align="center" height="100%" valign="top" width="100%" style="padding-bottom: 40px">
                          <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width:560px;">
                              <tr>
                                  <td align="center" valign="top" width="50%">
                                      <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                          <tr>
                                              <td style="text-align: center; padding: 0 10px;">
                                                  <a href="https:/www.google.com.co" target="_blank">
                                                  <img alt="pauta email" src="https://imagenes.mihv.com.co/email/200-200.png" width="200" height="200" alt="alt_text" class="center-on-narrow" style="border: 0;width: 100%;max-width: 200px; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                                  </a>
                                              </td>
                                          </tr>                                        
                                      </table>
                                  </td>
                                  <td align="center" valign="top" width="50%">
                                      <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                          <tr>
                                              <td style="text-align: center; padding: 0 10px;">
                                                  <a href="https:/www.google.com.co" target="_blank">
                                                  <img alt="pauta email" src="https://imagenes.mihv.com.co/email/200-200.png" width="200" height="200" alt="alt_text" class="center-on-narrow" style="border: 0;width: 100%;max-width: 200px; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                                  </a>
                                              </td>
                                          </tr>
                                          
                                      </table>
                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  <!-- Two Even Columns : END -->
            
              <!-- Email Footer : BEGIN -->
              <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;">
                  <tr>
                      <td style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center; color: #888888;">                        
                          <span class="mobile-link--footer"><a href="https://www.ingcoder.com" target="_blank">Mi HV (IngCoder SAS)</a></span><br><span class="mobile-link--footer">Carrera 13 # 13 - 24 Oficina 725<br>Bogotá D.C., C/marca, 110311 CO</span><br><span class="mobile-link--footer"><a href="tel:+573165269362"><img alt="whatsapp" src="https://imagenes.mihv.com.co/email/whatsapp.jpg" width="100%" style="max-width:9px;"> (+57) 316 526 9362</a></span>
                          <!--<br><br>
                          <unsubscribe style="color:#888888; text-decoration:underline;">unsubscribe</unsubscribe>-->
                      </td>
                  </tr>
              </table>
              <!-- Email Footer : END -->

              <!--[if mso]>
              </td>
              </tr>
              </table>
              <![endif]-->
          </div>
      </center>
  </body>
  </html>
  ';

  $subject = 'Resumen Diario de Ofertas - MI HV';

  $base = dirname(dirname(__FILE__));
  include_once($base . '/cron/PHPMailer-master/class.phpmailer.php');
  include_once($base . '/cron/PHPMailer-master/class.smtp.php');

  $mail = new PHPMailer;
  $mail->IsSMTP();
  $mail->SMTPAuth = true;
  $mail->SMTPSecure = "starttls";
  $mail->Host = "mail.mihv.co";
  $mail->Port = 587;
  $mail->Username = "informacion";
  $mail->Password = "Info2016*..*";
  $mail->From = "informacion@envios.mihv.co";
  $mail->FromName = "MI HV - Resumen Diario";
  $mail->Subject = $subject;
  $mail->AddAddress($data_email['email_ci']);
  $mail->MsgHTML(utf8_decode($html));
  $mail->IsHTML(true);
  //$mail->AddAttachment('../uploadAlDia/mi_archivo.jpg');
  
  if ($mail->Send()) {
    return true;
  }else {
    return false;
  }
}

