<?php

$base = dirname(dirname(__FILE__));
include_once($base . '/cron/functions_sql/functions_sql.php');
include_once($base . '/cron/base_path.php');

$life_cycle = 3;
$time = '30 DAY';
$time_spanish = '30';

$offers = model_load_offers_life_cycle($life_cycle, $time);
if (empty($offers)) {
  echo 'No hay ofertas urgentes que finalizar<br>';
}else {
  foreach ($offers as $key => $value) {

    $response_email = control_utilities_send_mail_user_offer_life_cycle_finalize($value['id_o'], $value['name_o'], $value['email_ci'], $value['name_ci'], $time_spanish);

    if ($response_email) {
      
      // Eliminar oferta
      $response = model_profile_company_remove_offer($value['id_o']);

      if($response == 1) {
        echo 'Se finalizo la oferta correctamente<br>';
      }else {
        echo 'Se envio el correo de notificacion de oferta finalizada pero no se elimino la oferta en la db<br>';

        $email_support = 'soporte@mihv.com.co';
        $from_name = 'MI HV - Fallo eliminar oferta urgente en la db por ciclo de vida';

        $subject = 'URGENTE! Fallo al eliminar oferta urgente de ciclo de vida en la db';
        $content ='
          <div>
            <p>Se envio el correo de notificacion de oferta urgente finalizada pero no se elimino la oferta en la db</p>
            <br>                
            <p>Id Oferta: ' . $value['id_o'] . '</p>
            <p>Nombre Oferta: ' . $value['name_o'] . '</p>
            <p>Id Empresa: ' . $value['id_ci'] . '</p>
            <p>Nombre Empresa: ' . $value['name_ci'] . '</p>
            <p>Correo Empresa: ' . $value['email_ci'] . '</p>
            <p>Ciclo de vida Actual: ' . $life_cycle . '</p>
          </div>
        ';

        $response_mail_support = control_utilities_send_mail_support($email_support, $content, $subject, $from_name);
        if ($response_mail_support) {
          echo 'Se envio correo a soporte soporte@mihv.com.co para que elimine la oferta urgente por el ciclo de vida<br>';
        }else {
          echo 'No se pudo enviar el correo a soporte soporte@mihv.com.co para que elimine la oferta urgente por el ciclo de vida<br>';
        }
      }  
    }
  }
}




function model_load_offers_life_cycle($life_cycle, $time){
  
  $sql = 'SELECT id_o, name_o, id_ci, email_ci, name_ci FROM tbl_oferts INNER JOIN tbl_company_info ON 
    tbl_oferts.tbl_company_info_id_ci=tbl_company_info.id_ci
    WHERE status_o = "OPEN" AND priority_o = "urgent" AND offer_life_cycle_o = ' . $life_cycle . ' 
    AND created_o <= NOW() - INTERVAL ' . $time . ';';

  $result = functions_sql_execute_query($sql);
  if (!empty($result)) {
    $rows = array();
    while($item = functions_sql_execute_get_dates($result)) {
      $rows[] = $item;
    }
    functions_sql_close_query_and_connection($result);
    return $rows;
  }
  functions_sql_close_query_and_connection($result);
  return FALSE;
}


/*
 * Envia correo de notificacion de oferta finalizada por el sistema
 */
function control_utilities_send_mail_user_offer_life_cycle_finalize($id_o, $name_o, $email_ci, $name_ci, $time) {

  $html = '';
  $html .='

  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
      <meta charset="utf-8"> <!-- utf-8 works for most cases -->
      <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldnt be necessary -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
      <title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->

      <style>

          /* What it does: Remove spaces around the email design added by some email clients. */
          /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
          html,
          body {
              margin: 0 auto !important;
              padding: 0 !important;
              height: 100% !important;
              width: 100% !important;
          }
          
          /* What it does: Stops email clients resizing small text. */
          * {
              -ms-text-size-adjust: 100%;
              -webkit-text-size-adjust: 100%;
          }
          
          /* What is does: Centers email on Android 4.4 */
          div[style*="margin: 16px 0"] {
              margin:0 !important;
          }
          
          /* What it does: Stops Outlook from adding extra spacing to tables. */
          table,
          td {
              mso-table-lspace: 0pt !important;
              mso-table-rspace: 0pt !important;
          }
                  
          /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
          table {
              border-spacing: 0 !important;
              border-collapse: collapse !important;
              table-layout: fixed !important;
              margin: 0 auto !important;
          }
          table table table {
              table-layout: auto; 
          }
          
          /* What it does: Uses a better rendering method when resizing images in IE. */
          img {
              -ms-interpolation-mode:bicubic;
          }
          
          /* What it does: A work-around for iOS meddling in triggered links. */
          .mobile-link--footer a,
          a[x-apple-data-detectors] {
              color:inherit !important;
              text-decoration: none !important;
          }
        
      </style>
      
      <!-- Progressive Enhancements -->
      <style>
          
          /* What it does: Hover styles for buttons */
          .button-td,
          .button-a {
              transition: all 100ms ease-in;
          }
          .button-td:hover,
          .button-a:hover {
              background: #555555 !important;
              border-color: #555555 !important;
          }


      </style>

  </head>
  <body width="100%" bgcolor="#ffffff" style="margin: 0; background: #ffffff;">
      <center style="width: 100%; background: #ffffff;">

          <!-- Visually Hidden Preheader Text : BEGIN -->
          <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
              Hola, tu oferta ' . $name_o . ' ha sido finalizada automaticamente.
          </div>
          <!-- Visually Hidden Preheader Text : END -->

          <!--    
              Set the email width. Defined in two places:
              1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 600px.
              2. MSO tags for Desktop Windows Outlook enforce a 600px width.
          -->
          <div style="max-width: 600px; margin: auto;">
              <!--[if mso]>
              <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="600" align="center">
              <tr>
              <td>
              <![endif]-->

              <!-- Email Header : BEGIN -->
              <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">
                  <tr>
                      <td style="padding: 20px 0; text-align: center;">
                        <img alt="notificacion oferta mi hv" src="https://imagenes.mihv.com.co/email/informacion-oferta-laboral.png" width="90%" alt="alt_text" border="0" style="background: none; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                          <br>
                          <img alt="Header mi hv" src="https://imagenes.mihv.com.co/email/header-logo.jpg" width="90%" height="50" alt="alt_text" border="0" style="background: none; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                      </td>
                  </tr>
              </table>
              <!-- Email Header : END -->
              
              <!-- Email Body : BEGIN -->
              <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">
                  
                  <!-- 1 Column Text + Button : BEGIN -->
                  <tr>
                      <td bgcolor="#ffffff">
                          <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                              <tr>
                                  <td style="padding: 40px; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                      Hola <b>' . $name_ci . '</b>
                                      <br><br>
                                      Queremos decirte que tu oferta <b>' . $name_o . '</b> ha finalizado ya que cumplió los <b>' . $time . ' días desde el inicio de la misma.
                                      <br>
                                      Esperamamos que hayas encontrado el personal para tu oferta.
                                      
                                      <!-- Button : END -->
                                      <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" style="margin: auto;" >
                                      <br>                                    
                                      <br>

                                      Si quieres publicar una nueva oferta dale click o ingresa a <a href="' . DOMAIN . '/views/profile_company.php" style="word-wrap: break-word;">
                                        ' . DOMAIN . '/views/profile_company.php
                                      </a>, logueate y publica.
                                      </table>
                                      <br><br>
                                      Gracias por contar con nosotros y nos veremos en otra ocasión;
                                      <br><br><br>
                                      El equipo de Mi HV.
                                      <br><br><br>
                                      PD: Posiblemente te interese...<br>
                                  </td>
                                  </tr>
                          </table>
                      </td>
                  </tr>
              </table>
              <!-- Email Body : END -->

              <!-- 2 Even Columns : BEGIN -->
                  <tr>                    
                      <td bgcolor="#ffffff" align="center" height="100%" valign="top" width="100%" style="padding-bottom: 40px">
                          <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width:560px;">
                              <tr>
                                  <td align="center" valign="top" width="50%">
                                      <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                          <tr>
                                              <td style="text-align: center; padding: 0 10px;">
                                                  <a href="https:/www.google.com.co" target="_blank">
                                                  <img alt="pauta email" src="https://imagenes.mihv.com.co/email/200-200.png" width="200" height="200" alt="alt_text" class="center-on-narrow" style="border: 0;width: 100%;max-width: 200px; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                                  </a>
                                              </td>
                                          </tr>                                        
                                      </table>
                                  </td>
                                  <td align="center" valign="top" width="50%">
                                      <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                          <tr>
                                              <td style="text-align: center; padding: 0 10px;">
                                                  <a href="https:/www.google.com.co" target="_blank">
                                                  <img alt="pauta email" src="https://imagenes.mihv.com.co/email/200-200.png" width="200" height="200" alt="alt_text" class="center-on-narrow" style="border: 0;width: 100%;max-width: 200px; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                                  </a>
                                              </td>
                                          </tr>
                                          
                                      </table>
                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  <!-- Two Even Columns : END -->
            
              <!-- Email Footer : BEGIN -->
              <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;">
                  <tr>
                      <td style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center; color: #888888;">                        
                          <span class="mobile-link--footer"><a href="https://www.ingcoder.com" target="_blank">Mi HV (IngCoder SAS)</a></span><br><span class="mobile-link--footer">Carrera 13 # 13 - 24 Oficina 725<br>Bogotá D.C., C/marca, 110311 CO</span><br><span class="mobile-link--footer"><a href="tel:+573165269362"><img alt="whatsapp" src="https://imagenes.mihv.com.co/email/whatsapp.jpg" width="100%" style="max-width:9px;"> (+57) 316 526 9362</a></span>
                          <!--<br><br>
                          <unsubscribe style="color:#888888; text-decoration:underline;">unsubscribe</unsubscribe>-->
                      </td>
                  </tr>
              </table>
              <!-- Email Footer : END -->

              <!--[if mso]>
              </td>
              </tr>
              </table>
              <![endif]-->
          </div>
      </center>
  </body>
  </html>
  ';

  $subject = 'Oferta Finalizada - MI HV';
  
  $base = dirname(dirname(__FILE__));
  include_once($base . '/cron/PHPMailer-master/class.phpmailer.php');
  include_once($base . '/cron/PHPMailer-master/class.smtp.php');

  $mail = new PHPMailer;
  $mail->IsSMTP();
  //permite modo debug para ver mensajes de las cosas que van ocurriendo
  //$mail->SMTPDebug = 2;
  $mail->SMTPAuth = true;
  $mail->SMTPSecure = "starttls";
  $mail->Host = "mail.mihv.co";
  $mail->Port = 587;
  $mail->Username = "informacion";
  $mail->Password = "Info2016*..*";
  $mail->From = "informacion@envios.mihv.co";    
  $mail->FromName = "MI HV - Oferta Finalizada";
  $mail->Subject = $subject;
  $mail->AddAddress($email_ci);
  $mail->MsgHTML(utf8_decode($html));
  $mail->IsHTML(true);
  //$mail->AddAttachment('../uploadAlDia/mi_archivo.jpg');
  
  if ($mail->Send()) {
    return true;
  }else {
    return false;
  }
}


function model_profile_company_remove_offer($id_o) {

  $sql = 'DELETE FROM tbl_oferts_apply WHERE tbl_oferts_id_o = ' . $id_o . ';';
  $result = functions_sql_execute_query($sql);

  $sql = "DELETE FROM tbl_oferts WHERE id_o = " . $id_o . " ;";
  $result = functions_sql_execute_query($sql);

  functions_sql_close_connection();
  
  return $result;
}

/*
 * Enviar correo a soporte
 */
function control_utilities_send_mail_support($email_support, $content, $subject, $from_name) {

  $html = '';
  $html .='

  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
      <meta charset="utf-8"> <!-- utf-8 works for most cases -->
      <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldnt be necessary -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
      <title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->
  </head>
  <body width="100%" bgcolor="#222222" style="margin: 0;">
      <center style="width: 100%; background: #ffffff;">
        ' . $content . '
      </center>
  </body>
  </html>
  ';

  //incluir libreria para correo
  $base = dirname(dirname(__FILE__)); // /Users/Mick/Sites/mihv/system   - cron/cron_delete_user_inactive.php
  include_once($base . '/cron/PHPMailer-master/class.phpmailer.php');
  include_once($base . '/cron/PHPMailer-master/class.smtp.php');
  
  $mail = new PHPMailer;
  $mail->IsSMTP();
  //permite modo debug para ver mensajes de las cosas que van ocurriendo
  //$mail->SMTPDebug = 2;
  $mail->SMTPAuth = true;
  $mail->SMTPSecure = "starttls";
  $mail->Host = "mail.mihv.co";
  $mail->Port = 587;
  $mail->Username = "informacion";
  $mail->Password = "Info2016*..*";
  $mail->From = "informacion@envios.mihv.co";   
  $mail->FromName = $from_name;
  $mail->Subject = $subject;
  $mail->AddAddress($email_support);
  $mail->MsgHTML(utf8_decode($html));
  $mail->IsHTML(true);
  //$mail->AddAttachment('../uploadAlDia/mi_archivo.jpg');
  
  if ($mail->Send()) {
    return true;
  }else {
    return false;
  }
}
