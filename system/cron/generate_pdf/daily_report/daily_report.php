<?php


// Include the main TCPDF library (search for installation path).
$base = dirname(dirname(__FILE__));
include_once($base . '/daily_report/daily_report_inc.php');
include_once($base . '/../../cron/functions_sql/functions_sql.php');
include_once($base . '/../../cron/base_path.php'); // global -> PATH_DIR_DAILY_REPORT


$status_daily_report = 2; // 2 = en proceso
$status_where = 0; // 0 = no generado
$update_data_bill = update_status_daily_report($status_daily_report, $status_where);

if (!$update_data_bill) {
  echo 'no se pudo actualizar el estado de daily report a: 2';
  exit;
}

$data_bill = get_all_bill($status_daily_report);

if (empty($data_bill)) {
  $status_daily_report = 0; // 0 = no generado
  $status_where = 2; // 2 = en proceso
  $update_data_bill = update_status_daily_report($status_daily_report, $status_where);
  echo 'No hay facturas para generar informe diario';
  exit;
}

// crear nuevo registro tbl daily report
$mysqli = conexion();

$sql = "INSERT INTO tbl_system_daily_report(date_sdr) VALUES (?)";

if (!($sentence = $mysqli->prepare($sql))) {
  $status_daily_report = 0; // 0 = no generado
  $status_where = 2; // 2 = en proceso
  $update_data_bill = update_status_daily_report($status_daily_report, $status_where);
   echo "Falló la preparación: (" . $mysqli->errno . ") " . $mysqli->error;
}

$date_current = date('Y-m-d h:i:s');

if (!$sentence->bind_param('s', $date_current)) {
  $status_daily_report = 0; // 0 = no generado
  $status_where = 2; // 2 = en proceso
  $update_data_bill = update_status_daily_report($status_daily_report, $status_where);
  echo "Falló la vinculación de parámetros: (" . $sentence->errno . ") " . $sentence->error;
}

if (!$sentence->execute()) {
  $status_daily_report = 0; // 0 = no generado
  $status_where = 2; // 2 = en proceso
  $update_data_bill = update_status_daily_report($status_daily_report, $status_where);
  echo "Falló la ejecución: (" . $sentence->errno . ") " . $sentence->error;
}

functions_sql_close_connection();
  
if (empty($sentence->insert_id)) {
  $status_daily_report = 0; // 0 = no generado
  $status_where = 2; // 2 = en proceso
  $update_data_bill = update_status_daily_report($status_daily_report, $status_where);
  exit;
}

$id_daily = $sentence->insert_id;


//$total_data_bill = count($data_bill);
 
$min_id_bill = get_position_id_bill('MIN', $status_daily_report);
$max_id_bill = get_position_id_bill('MAX', $status_daily_report);

function process_tbl_daily_report($id_daily) {
  $auto_increment = $id_daily - 1;
  $sql = 'DELETE FROM tbl_system_daily_report WHERE id_sdr = ' . $id_daily . ';';
  functions_sql_execute_query($sql);
  $sql = 'ALTER TABLE tbl_system_daily_report AUTO_INCREMENT = ' . $auto_increment . ';';
  functions_sql_execute_query($sql);
}

function get_all_bill($status_daily_report) {
  $sql = 'SELECT * FROM tbl_system_billing WHERE daily_report_sb = ' . $status_daily_report . ';';
  return execute_sql_get($sql);
}

function get_position_id_bill($op, $status_daily_report) {
  $sql = 'SELECT ' . $op . '(id_sb) FROM tbl_system_billing WHERE daily_report_sb = ' . $status_daily_report . ';';
  return execute_sql_get($sql);
}

function update_status_daily_report($status_daily_report, $status_where) {
  $sql = 'UPDATE tbl_system_billing SET daily_report_sb = ' . $status_daily_report . ' WHERE daily_report_sb = ' . $status_where . ';';
  return functions_sql_execute_query($sql);
}


function execute_sql_get($sql) {
  $result = functions_sql_execute_query($sql);
  if (!empty($result)) {
    $rows = array();
    while($item = functions_sql_execute_get_dates($result)) {
      $rows[] = $item;
    }
    functions_sql_close_query_and_connection($result);
    return $rows;
  }
  functions_sql_close_query_and_connection($result);
  return FALSE;
}

// create new PDF document
class MYPDF extends TCPDF {

  //Page header
  public function Header() {
    $this->SetFont('helvetica', 'B', 8.5);
    $this->MultiCell(21, 8, 'Factura', 1, 'C', 0, 0, 15, '', true, 0, false, true, 8, 'M');
  	$this->MultiCell(24, 8, 'Valor Base', 1, 'C', 0, 0, '', '', true, 0, false, true, 8, 'M');
  	$this->MultiCell(21, 8, "Servicios\nIVA 19%", 1, 'C', 0, 0, '', '', true, 0, false, true, 8, 'M');
  	$this->MultiCell(19, 8, 'Descuentos', 1, 'C', 0, 0, '', '', true, 0, false, true, 8, 'M');
  	$this->MultiCell(12, 8, 'Exento de IVA', 1, 'C', 0, 0, '', '', true, 0, false, true, 8, 'M');
  	$this->MultiCell(16, 8, "Servicios\nIVA 8%", 1, 'C', 0, 0, '', '', true, 0, false, true, 8, 'M');
  	$this->MultiCell(16, 8, "Consumo\nIVA 16%", 1, 'C', 0, 0, '', '', true, 0, false, true, 8, 'M');
  	$this->MultiCell(16, 8, "Consumo\nIVA 8%", 1, 'C',  0, 0, '', '', true, 0, false, true, 8, 'M');
  	$this->MultiCell(16, 8, "Consumo\nIVA 4%", 1, 'C',  0, 0, '', '', true, 0, false, true, 8, 'M');
  	$this->MultiCell(25, 8, 'Total Factura', 1, 'C', 0, 1, '', '', true, 0, false, true, 8, 'M');
  }
}

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Codection SAS');
$pdf->SetTitle('Informe diario');
$pdf->SetSubject('Comprobante de informe diario');

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER+66);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/spa-informe.php')) {
	require_once(dirname(__FILE__).'/lang/spa-informe.php');
	$pdf->setLanguageArray($l);
}

// -----------------------------------------------------

// set font
$pdf->AddPage();

$linea_vertical=0;

// INICIO PDF
//Texto en negrita
$pdf->SetFont('helvetica', 'B', 11);
//Texto Emisor
$linea_vertical=($linea_vertical+9.4);
$pdf->MultiCell(93, 0, PDF_TEXT_BOLD_PROVIDER."\n", 0, 'J', false, 1, 15, $linea_vertical, true, 0, false, true, 0, 'M');
$pdf->MultiCell(93, 0, PDF_TEXT_BOLD_IT_PROVIDER."\n", 0, 'J', false, 1, 118, $linea_vertical, true, 0, false, true, 0, 'M');

$pdf->SetFont('helvetica', '', 11);
$linea_vertical=($linea_vertical+6);
$pdf->MultiCell(93, 0, PDF_AUTHOR.PDF_AUTHOR_INITIAL."\n", 0, 'J', false, 1, 15, $linea_vertical, true, 0, false, true, 0, 'M');
$pdf->MultiCell(93, 0, PDF_AUTHOR_IT."\n", 0, 'J', false, 1, 118, $linea_vertical, true, 0, false, true, 0, 'M');

$pdf->SetFont('helvetica', 'B', 11);
$linea_vertical=($linea_vertical+9);
$pdf->MultiCell(93, 0, PDF_TEXT_BOLD_PLACE_PRINTING_DATE."\n", 0, 'J', false, 1, 15, $linea_vertical, true, 0, false, true, 0, 'M');
$pdf->MultiCell(93, 0, PDF_TEXT_BOLD_PC_ID."\n", 0, 'J', false, 1, 118, $linea_vertical, true, 0, false, true, 0, 'M');

$pdf->SetFont('helvetica', '', 11);
$linea_vertical=($linea_vertical+6);
$pdf->MultiCell(93, 0, PDF_AUTHOR_CITY.", ".PDF_DATE."\n", 0, 'J', false, 1, 15, $linea_vertical, true, 0, false, true, 0, 'M');
$pdf->MultiCell(93, 0, PDF_PC_ID."\n", 0, 'J', false, 1, 118, $linea_vertical, true, 0, false, true, 0, 'M');

$pdf->SetFont('helvetica', 'B', 11);
$linea_vertical=($linea_vertical+9);
$pdf->MultiCell(141, 0, PDF_TEXT_BOLD_DIAN_DISCRIMINATION_DEPARTMENT, 0, 'J', false, 1, 15, $linea_vertical, true, 0, false, true, 0, 'M');
$pdf->SetFont('helvetica', 'BU', 11);
$pdf->MultiCell(45, 0, PDF_TEXT_DIAN_DISCRIMINATION_DEPARTMENT."\n", 0, 'J', false, 1, 156, $linea_vertical, true, 0, false, true, 0, 'M');

$pdf->SetFont('helvetica', 'B', 11);
$linea_vertical=($linea_vertical+10);
$pdf->MultiCell(186, 0, PDF_TEXT_BOLD_DAILY_TRANSACTION."\n", 0, 'J', false, 1, 15, $linea_vertical, true, 0, false, true, 0, 'M');

$linea_vertical=($linea_vertical+7);
$pdf->MultiCell(39, 0, PDF_TEXT_BOLD_INITIAL_DAILY_TRANSACTION, 0, 'J', false, 1, 15, $linea_vertical, true, 0, false, true, 0, 'M');
$pdf->MultiCell(37, 0, PDF_TEXT_BOLD_LAST_DAILY_TRANSACTION, 0, 'J', false, 1, 109, $linea_vertical, true, 0, false, true, 0, 'M');

$pdf->SetFont('helvetica', '', 11);
$pdf->MultiCell(50, 0, $min_id_bill[0]['MIN(id_sb)'] ."\n", 0, 'J', false, 1, 54, $linea_vertical, true, 0, false, true, 0, 'M');
$pdf->MultiCell(50, 0, $max_id_bill[0]['MAX(id_sb)'] ."\n", 0, 'J', false, 1, 146, $linea_vertical, true, 0, false, true, 0, 'M');


$pdf->SetFont('helvetica', 'B', 11);
$linea_vertical=($linea_vertical+10);
$pdf->MultiCell(186, 0, PDF_TEXT_BOLD_DIAN_DISCRIMINATION_PC."\n", 0, 'J', false, 1, 15, $linea_vertical, true, 0, false, true, 0, 'M');
$pdf->Ln(12);

$pdf->SetFont('helvetica', '', 10);//Para los datos de la DB


$pdf->SetHeaderMargin(14);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

$id_sb = 0;
$count_pay_balance = 0; //Calculo del total de transaciones opcion saldo cuenta
$base_dian = 0; //Calculo del total de transaciones base
$iva_dian = 0; //Calculo del total de transaciones iva
$total_dian = 0; //Calculo del total de transaciones total

foreach ($data_bill as $row) {

  $total = $row['base_value_o'] + $row['iva_o'];
  $id_sb = $row['id_sb'];

  $pdf->MultiCell(21, 0, $row['id_sb'] .'', 1, 'R', 0, 0, 15, '', true, 0, false, true, 0);
  $pdf->MultiCell(24, 0, '$'. $row['base_value_o'] .'', 1, 'R', 0, 0, 36, '', true, 0, false, true, 0);
  $pdf->MultiCell(21, 0, '$'. $row['iva_o'] .'', 1, 'R', 0, 0, 60, '', true, 0, false, true, 0);
  $pdf->MultiCell(19, 0, '$0', 1, 'C', 0, 0, 81, '', true, 0, false, true, 0);
  $pdf->MultiCell(12, 0, '$0', 1, 'C', 0, 0, 100, '', true, 0, false, true, 0);
  $pdf->MultiCell(16, 0, '$0', 1, 'C', 0, 0, 112, '', true, 0, false, true, 0);
  $pdf->MultiCell(16, 0, '$0', 1, 'C', 0, 0, 128, '', true, 0, false, true, 0);
  $pdf->MultiCell(16, 0, '$0', 1, 'C', 0, 0, 144, '', true, 0, false, true, 0);
  $pdf->MultiCell(16, 0, '$0', 1, 'C', 0, 0, 160, '', true, 0, false, true, 0);
  $pdf->MultiCell(25, 0, '$'. $total .'', 1, 'R', 0, 1, 176, '', true, 0, false, true, 0);

  //Contador de trasacciones
	if ($row['pay_method_o'] = 7) { // Metodo de pago por Saldo(recarga)
	  $count_pay_balance++;
	}

  //Calculos para la DIAN		
	$base_dian  = $base_dian + $row['base_value_o'];
	$iva_dian   = $iva_dian + $row['iva_o'];
	$total_dian = $total_dian + $total;


	$lulu = 0;
	$lulu = $pdf->getY();
	$lulu = (int)$lulu;

	if ($lulu>264) {
		$pdf->endPage();			
		$pdf->AddPage();
		$pdf->lastPage();
		$pdf->Ln(10);
  }
			
}//End Foreach

if ($id_sb == $max_id_bill[0]['MAX(id_sb)']) {
  $pdf->lastPage();
  $pdf->SetPrintHeader(false);
}

$pdf->MultiCell(0, 0, "", 0, 'C', 0, 1, '', '', true, 0, false, true, 0);

/*Esto con 119 registros
  104=255 lolo - Titulo tabla 1
  105=256 lolo

  95=255 lolo2 - Tabla 1
  96=256 lolo3

  82=255 lolo4 - Titulo tabla 2
  83=256 lolo4	
  	
  69=251 lolo5 - tabla 2 - Limite
  70=252

  58=255 lolo6 - Serial PC
  59=266

  49=255 lolo6 - Ubicacion PC
  50=266
  */

//$pdf->Ln(50); //espaciados



//Parte final del PDF	
$lolo = $pdf->getY();
$lolo = (int)$lolo;

if ($lolo > 255) {
	$pdf->endPage();			
	$pdf->AddPage();
	$pdf->lastPage();
}

$pdf->Ln('');
$pdf->SetFont('helvetica', 'B', 10);//Para título de la totalización
$pdf->MultiCell(0, 0, "Medios de recaudación para las facturas\n", 0, 'C', 0, 1, 15, '', true, 0, false, true, 0);

if ($lolo > 255) {
	$lolo2 = 0;
} else{
	$lolo2 = $pdf->getY();
	$lolo2 = (int)$lolo2;
}

if ($lolo2 >= 255) {
  $pdf->endPage();			
  $pdf->AddPage();	
  $pdf->lastPage();	
}

$pdf->Ln('');
$pdf->SetFont('helvetica', 'B', 9);
$pdf->MultiCell(20, 0, 'Efectivo', 1, 'C', 0, 0, '', '', true, 0, false, true, 0, 'M');
$pdf->MultiCell(20, 0, 'Cheques', 1, 'C', 0, 0, '', '', true, 0, false, true, 0, 'M');
$pdf->MultiCell(46, 0, "Tarjetas débito o crédito", 1, 'C', 0, 0, '', '', true, 0, false, true, 0, 'M');
$pdf->MultiCell(30, 0, 'Venta a crédito', 1, 'C', 0, 0, '', '', true, 0, false, true, 0, 'M');
$pdf->MultiCell(15, 0, 'Bonos', 1, 'C', 0, 0, '', '', true, 0, false, true, 0, 'M');
$pdf->MultiCell(15, 0, 'Vales', 1, 'C', 0, 0, '', '', true, 0, false, true, 0, 'M');
$pdf->MultiCell(40, 0, 'Otros (Saldo de cuenta)', 1, 'C', 0, 0, '', '', true, 0, false, true, 0, 'M');

$pdf->Ln('');
$pdf->SetFont('helvetica', '', 9);
$pdf->MultiCell(20, 0, '0', 1, 'C', 0, 0, '', '', true, 0, false, true, 0, 'M');
$pdf->MultiCell(20, 0, '0', 1, 'C', 0, 0, '', '', true, 0, false, true, 0, 'M');
$pdf->MultiCell(46, 0, '0', 1, 'C', 0, 0, '', '', true, 0, false, true, 0, 'M');
$pdf->MultiCell(30, 0, '0', 1, 'C', 0, 0, '', '', true, 0, false, true, 0, 'M');
$pdf->MultiCell(15, 0, '0', 1, 'C', 0, 0, '', '', true, 0, false, true, 0, 'M');
$pdf->MultiCell(15, 0, '0', 1, 'C', 0, 0, '', '', true, 0, false, true, 0, 'M');
$pdf->MultiCell(40, 0, $count_pay_balance, 1, 'C', 0, 1, '', '', true, 0, false, true, 0, 'M');


if ($lolo2>255) {
	$lolo4=0;
}
else{
	$lolo4=0;
	$lolo4=$pdf->getY();
	$lolo4=(int)$lolo4;
	}

if ($lolo4>=256)	
{	
$pdf->endPage();			
$pdf->AddPage();
$pdf->lastPage();	
}

//Sección valor total
$pdf->Ln('');	
$pdf->SetFont('helvetica', 'B', 10);//Para título del valor a pagar
$pdf->MultiCell(0, 0, "Valor total de lo registrado\n", 0, 'C', 0, 1, 15, '', true, 0, false, true, 0);	

if ($lolo4>255) {
	$lolo5=0;
}
else{
	$lolo5=0;
	$lolo5=$pdf->getY();
	$lolo5=(int)$lolo5;	
	}

if ($lolo5>251) //lim 251	
{
$pdf->endPage();			
$pdf->AddPage();	
$pdf->lastPage();	
}

$pdf->Ln('');		
$pdf->SetFont('helvetica', 'B', 8.5);
$pdf->MultiCell(29, 8, 'Valor Base', 1, 'C', 0, 0, '', '', true, 0, false, true, 8, 'M');
$pdf->MultiCell(29, 8, "Servicios IVA 19%", 1, 'C', 0, 0, '', '', true, 0, false, true, 8, 'M');
$pdf->MultiCell(20, 8, 'Descuentos', 1, 'C', 0, 0, '', '', true, 0, false, true, 8, 'M');
$pdf->MultiCell(13, 8, 'Exento de IVA', 1, 'C', 0, 0, '', '', true, 0, false, true, 8, 'M');	
$pdf->MultiCell(17, 8, "Servicios\nIVA 8%", 1, 'C', 0, 0, '', '', true, 0, false, true, 8, 'M');	
$pdf->MultiCell(17, 8, 'Consumo IVA 16%', 1, 'C', 0, 0, '', '', true, 0, false, true, 8, 'M');
$pdf->MultiCell(16, 8, 'Consumo IVA 8%', 1, 'C', 0, 0, '', '', true, 0, false, true, 8, 'M');
$pdf->MultiCell(16, 8, 'Consumo IVA 4%', 1, 'C',  0, 0, '', '', true, 0, false, true, 8, 'M');
$pdf->MultiCell(29, 8, 'Total Factura', 1, 'C', 0, 0, '', '', true, 0, false, true, 8, 'M');	
//Variables de la DIAN
$pdf->Ln('');
$pdf->SetFont('helvetica', 'B', 8.5);
$pdf->MultiCell(29, 0, '$'.$base_dian, 1, 'C', 0, 0, '', '', true, 0, false, true, 0, 'M');
$pdf->MultiCell(29, 0, '$'.$iva_dian, 1, 'C', 0, 0, '', '', true, 0, false, true, 0, 'M');
$pdf->MultiCell(20, 0, '$0', 1, 'C', 0, 0, '', '', true, 0, false, true, 0, 'M');
$pdf->MultiCell(13, 0, '$0', 1, 'C', 0, 0, '', '', true, 0, false, true, 0, 'M');	
$pdf->MultiCell(17, 0, '$0', 1, 'C', 0, 0, '', '', true, 0, false, true, 0, 'M');	
$pdf->MultiCell(17, 0, '$0', 1, 'C', 0, 0, '', '', true, 0, false, true, 0, 'M');
$pdf->MultiCell(16, 0, '$0', 1, 'C', 0, 0, '', '', true, 0, false, true, 0, 'M');
$pdf->MultiCell(16, 0, '$0', 1, 'C',  0, 0, '', '', true, 0, false, true, 0, 'M');
$pdf->MultiCell(29, 0, '$'.$total_dian, 1, 'C', 0, 1, '', '', true, 0, false, true, 0, 'M');

if ($lolo5>255) {
	$lolo6=0;
}
else{
	$lolo6=0;
	$lolo6=$pdf->getY();
	$lolo6=(int)$lolo6;	
	}

if ($lolo6>255) //lim 255	
{
$pdf->endPage();			
$pdf->AddPage();	
$pdf->lastPage();	
}
$pdf->Ln('');
$pdf->SetFont('helvetica', 'B', 11);
$pdf->MultiCell(45, 0, PDF_TEXT_BOLD_PC_SERIAL_NUMBER, 0, 'L',  0, 0, 15, '', true, 0, false, true, 0, 'M');
$pdf->SetFont('helvetica', '', 11);
$pdf->MultiCell(0, 0, PDF_TEXT_PC_SERIAL_NUMBER, 0, 'L', 0, 1, '', '', true, 0, false, true, 0, 'M');

if ($lolo6>255) {
	$lolo7=0;
}
else{
	$lolo7=0;
	$lolo7=$pdf->getY();
	$lolo7=(int)$lolo7;	
	}

if ($lolo7>255) //lim 255	
{
$pdf->endPage();			
$pdf->AddPage();	
$pdf->lastPage();	
}
$pdf->Ln('');
$pdf->SetFont('helvetica', 'B', 11);
$pdf->MultiCell(53, 0, PDF_TEXT_BOLD_PC_UBICATION, 0, 'L',  0, 0, 15, '', true, 0, false, true, 0, 'M');
$pdf->SetFont('helvetica', '', 11);
$pdf->MultiCell(0, 0, PDF_TEXT_PC_UBICATION, 0, 'L', 0, 1, '', '', true, 0, false, true, 0, 'M');


//Close and output PDF document
$pdf->Output(PATH_DIR_DAILY_REPORT . '/informe-diario_' . $id_daily . '.pdf', 'F');

if(file_exists(PATH_DIR_DAILY_REPORT . '/informe-diario_' . $id_daily . '.pdf')) {
  $status_daily_report = 1; // 1 = Generado
  $status_where = 2; // 2 = en proceso
  $update_data_bill = update_status_daily_report($status_daily_report, $status_where);

}else {
  process_tbl_daily_report($id_daily);
  $status_daily_report = 0; // 0 = no generado
  $status_where = 2; // 2 = en proceso
  $update_data_bill = update_status_daily_report($status_daily_report, $status_where);
}

// 
//$pdf->Output('Informe diario numero'.$id_daily.'.pdf', 'I');

