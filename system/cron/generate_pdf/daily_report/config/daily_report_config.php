<?php

//Fecha Actual
$fechaactual = getdate();
$dia=$fechaactual["mday"];
$tiempo=$fechaactual["year"];
$mes=$fechaactual["month"];
$hora=$fechaactual["hours"];
$minutos=$fechaactual["minutes"];
$segundos=$fechaactual["seconds"];
$franja="";
//Pasar variable mes ingles a español
    if ($mes=="January") $mes="Enero";
    if ($mes=="February") $mes="Febrero";
    if ($mes=="March") $mes="Marzo";
    if ($mes=="April") $mes="Abril";
    if ($mes=="May") $mes="Mayo";
    if ($mes=="June") $mes="Junio";
    if ($mes=="July") $mes="Julio";
    if ($mes=="August") $mes="Agosto";
    if ($mes=="September") $mes="Setiembre";
    if ($mes=="October") $mes="Octubre";
    if ($mes=="November") $mes="Noviembre";
    if ($mes=="December") $mes="Diciembre";
    
    //Cambio Hora
    if ($hora=="0"){$franja="AM";$hora="00";}
    if ($hora=="1"){$franja="AM";$hora="01";}
    if ($hora=="2"){$franja="AM";$hora="02";}
    if ($hora=="3"){$franja="AM";$hora="03";}
    if ($hora=="4"){$franja="AM";$hora="04";}
    if ($hora=="5"){$franja="AM";$hora="05";}
    if ($hora=="6"){$franja="AM";$hora="06";}
    if ($hora=="7"){$franja="AM";$hora="07";}
    if ($hora=="8"){$franja="AM";$hora="08";}
    if ($hora=="9"){$franja="AM";$hora="09";}
    if ($hora=="10"){$franja="AM";}
    if ($hora=="11"){$franja="AM";}
    if ($hora=="12"){$franja="PM";}
    if ($hora=="13"){$franja="PM";$hora="01";}
    if ($hora=="14"){$franja="PM";$hora="02";}
    if ($hora=="15"){$franja="PM";$hora="03";}
    if ($hora=="16"){$franja="PM";$hora="04";}
    if ($hora=="17"){$franja="PM";$hora="05";}
    if ($hora=="18"){$franja="PM";$hora="06";}
    if ($hora=="19"){$franja="PM";$hora="07";}
    if ($hora=="20"){$franja="PM";$hora="08";}
    if ($hora=="21"){$franja="PM";$hora="09";}
    if ($hora=="22"){$franja="PM";$hora="10";}
    if ($hora=="23"){$franja="PM";$hora="11";}

    //Cambio Minutos
    if ($minutos=="0"){$minutos="00";}
    if ($minutos=="1"){$minutos="01";}
    if ($minutos=="2"){$minutos="02";}
    if ($minutos=="3"){$minutos="03";}
    if ($minutos=="4"){$minutos="04";}
    if ($minutos=="5"){$minutos="05";}
    if ($minutos=="6"){$minutos="06";}
    if ($minutos=="7"){$minutos="07";}
    if ($minutos=="8"){$minutos="08";}
    if ($minutos=="9"){$minutos="09";}

    //Cambio Segundos
    if ($segundos=="0"){$segundos="00";}
    if ($segundos=="1"){$segundos="01";}
    if ($segundos=="2"){$segundos="02";}
    if ($segundos=="3"){$segundos="03";}
    if ($segundos=="4"){$segundos="04";}
    if ($segundos=="5"){$segundos="05";}
    if ($segundos=="6"){$segundos="06";}
    if ($segundos=="7"){$segundos="07";}
    if ($segundos=="8"){$segundos="08";}
    if ($segundos=="9"){$segundos="09";}

//Variable fecha español
define ('PDF_DATE',"$dia de $mes de $tiempo - $hora:$minutos:$segundos $franja");


/*
 DATOS DEL AUTOR. //LLAMAR DE LA DB
 */
define ('PDF_AUTHOR', "Code Connection Professional SAS");
define ('PDF_AUTHOR_INITIAL',' - Codection');
define ('PDF_AUTHOR_IT', "NIT 901019412-3");
define ('PDF_AUTHOR_COMMON_SYSTEM', "Régimen Común - No somos grandes contribuyentes");
define ('PDF_AUTHOR_ADDRESS',"Carrera 13 # 13 - 24 Oficina 725 Edificio Lara");
define ('PDF_AUTHOR_ADDRESS_ZIPCODE',"110311");
define ('PDF_AUTHOR_CITY',"Bogotá D.C.");
define ('PDF_AUTHOR_COUNTRY',"Colombia");
define ('PDF_AUTHOR_CONTACT_WEB',"www.codection.pro");
define ('PDF_AUTHOR_CONTACT_EMAIL',"factura@codection.pro");

//Título del documento (ref)
define ('PDF_DOCUMENT_INFO', 'Comprobante de Informe Diario N° ');
/*
DATOS DIAN PROVEEDOR LLamar a la db
*/
define ('PDF_DIAN_RESOLUTION','Resolución DIAN N° 18762001429781 del 5/12/2016');
define ('PDF_DIAN_AUTORIZATION','Numeración autorizada por la DIAN 1 al 10000');
define ('PDF_DIAN_INVOICE_TYPE','Comprobante de informe diario (ORIGINAL) creada por:');
/* DATOS DEL EDITOR DE LA FACTURA */
define ('PDF_EDITOR','Code Connection Professional SAS');
define ('PDF_EDITOR_IT','NIT 901019412-3');

/*
FIN VARIABLES DE LLAMADO A LA DB ENCABEZADO
*/


/*
------------------------------------------------------------------------
INICIO CUERPO PDF
------------------------------------------------------------------------
*/

//Nombre de la maquina
define ('PDF_PC_ID',gethostname());

//Ciudad de Edición
define ('PDF_EDITION_CITY','Bogotá D.C');


define ('PDF_TEXT_BOLD_PROVIDER','Nombre o razón social del prestador de servicio:');

define ('PDF_TEXT_BOLD_IT_PROVIDER','Identificación Tributaria:');

define ('PDF_TEXT_BOLD_PLACE_PRINTING_DATE','Lugar, fecha y hora de la impresión:');

define ('PDF_TEXT_BOLD_PC_ID','Identificación del computador:');

define ('PDF_TEXT_BOLD_DIAN_DISCRIMINATION_DEPARTMENT','Discriminación de las venta por prestación de servicios por departamento: ');

define ('PDF_TEXT_DIAN_DISCRIMINATION_DEPARTMENT','No Aplica');

define ('PDF_TEXT_BOLD_DAILY_TRANSACTION','Discriminación de las ventas por prestación de servicios del computador: ');

define ('PDF_TEXT_BOLD_PC_SERIAL_NUMBER','Serial del computador:');

define ('PDF_TEXT_PC_SERIAL_NUMBER','VMware-42 06 90 15 cc c7 9f 00-29 bd 67 9c ee a2 ef 01');

define ('PDF_TEXT_BOLD_PC_UBICATION','Ubicación del computador:');

define ('PDF_TEXT_PC_UBICATION','Montreal - Canada');

define ('PDF_TEXT_BOLD_INITIAL_DAILY_TRANSACTION','Transacción Inicial: ');

define ('PDF_TEXT_BOLD_LAST_DAILY_TRANSACTION','Transacción Final: ');

define ('PDF_TEXT_BOLD_DIAN_DISCRIMINATION_PC','Discriminación de las ventas generadas del computador '.PDF_PC_ID.' con las transacciones atendidas y su respectivo valor por la prestación del servicio:');

/*
------------------------------------------------------------------------
FIN CUERPO PDF
------------------------------------------------------------------------
*/


/*
 DATOS DE CONFIGURACIÓN PREESTABLECIDOS
 */
define ('K_TCPDF_EXTERNAL_CONFIG', true);

define ('K_PATH_IMAGES', dirname(__FILE__).'/../images/');

/**
 * Deafult image logo used be the default Header() method.
 * Please set here your own logo or an empty string to disable it.
 */
define ('PDF_HEADER_LOGO', '_blank.png');

/**
 * Header logo image width in user units.
 */
define ('PDF_HEADER_LOGO_MARGIN',0);

/**
 * Cache directory for temporary files (full path).
 */
define ('K_PATH_CACHE', sys_get_temp_dir().'/');

/**
 * Generic name for a blank image.
 */
define ('K_BLANK_IMAGE', '_blank.png');

/**
 * Page format.
 */
define ('PDF_PAGE_FORMAT', 'LETTER');

/**
 * Page orientation (P=portrait, L=landscape).
 */
define ('PDF_PAGE_ORIENTATION', 'P');

/**
 * Document creator.
 */
define ('PDF_CREATOR', 'TCPDF');



/**
 * Document unit of measure [pt=point, mm=millimeter, cm=centimeter, in=inch].
 */
define ('PDF_UNIT', 'mm');

/**
 * Header margin.
 */
define ('PDF_MARGIN_HEADER', 14);

/**
 * Footer margin.
 */
define ('PDF_MARGIN_FOOTER', 8.2);

/**
 * Top margin.
 */
define ('PDF_MARGIN_TOP', 12);

/**
 * Bottom margin.
 */
define ('PDF_MARGIN_BOTTOM', 11.5);

/**
 * Left margin.
 */
define ('PDF_MARGIN_LEFT', 15);

/**
 * Right margin.
 */
define ('PDF_MARGIN_RIGHT', 15);

/**
 * Default main font name.
 */
define ('PDF_FONT_NAME_MAIN', 'helvetica');

/**
 * Default main font size.
 */
define ('PDF_FONT_SIZE_MAIN', 10);

/**
 * Default data font name.
 */
define ('PDF_FONT_NAME_DATA', 'helvetica');

/**
 * Default data font size.
 */
define ('PDF_FONT_SIZE_DATA', 8);

/**
 * Default monospaced font name.
 */
define ('PDF_FONT_MONOSPACED', 'courier');

/**
 * Ratio used to adjust the conversion of pixels to user units.
 */
define ('PDF_IMAGE_SCALE_RATIO', 1.25);

/**
 * Magnification factor for titles.
 */
define('HEAD_MAGNIFICATION', 1.1);

/**
 * Height of cell respect font height.
 */
define('K_CELL_HEIGHT_RATIO', 1.25);

/**
 * Title magnification respect main font size.
 */
define('K_TITLE_MAGNIFICATION', 1.3);

/**
 * Reduction factor for small font.
 */
define('K_SMALL_RATIO', 2/3);

/**
 * Set to true to enable the special procedure used to avoid the overlappind of symbols on Thai language.
 */
define('K_THAI_TOPCHARS', true);

/**
 * If true allows to call TCPDF methods using HTML syntax
 * IMPORTANT: For security reason, disable this feature if you are printing user HTML content.
 */
define('K_TCPDF_CALLS_IN_HTML', true);

/**
 * If true and PHP version is greater than 5, then the Error() method throw new exception instead of terminating the execution.
 */
define('K_TCPDF_THROW_EXCEPTION_ERROR', false);

//============================================================+
// END OF FILE
//============================================================+
