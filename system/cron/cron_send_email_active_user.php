<?php
$base = dirname(dirname(__FILE__));
include_once($base . '/cron/functions_sql/functions_sql.php');
include_once($base . '/cron/base_path.php');

$type_user = array(
  'person' => array(
    'table' => 'person',
    'prefix' => 'pi',
    'fields' => 'id_pi, name_pi, email_pi',
    'type_token' => 'PERSON',
  ), 
  'company' => array(
    'table' => 'company',
    'prefix' => 'ci',
    'fields' => 'id_ci, name_ci, email_ci',
    'type_token' => 'COMPANY',
  ),
);

$time = '6 HOUR';

foreach ($type_user as $key_type => $value) {

  $users_inactived = model_user_get_user_inactive($value['table'], $value['prefix'], $value['fields'], $time);

  if (empty($users_inactived)) {
    echo 'No hay usuarios ' . $value['table'] . ' por activar<br>';
  }else {
    foreach ($users_inactived as $key_user => $value_user) {
      
      $token_active_user = md5($value_user['name_' . $value['prefix']] . '_' . mt_rand());

      $update_token = model_user_update_token_active_user($value['table'], $value['prefix'], $value_user['id_' . $value['prefix']], $token_active_user);

      if ($update_token == 1) {
        $response_mail_activate = control_utilities_send_mail_user_register($value['type_token'], $token_active_user, $value_user['name_' . $value['prefix']], $value_user['email_' . $value['prefix']]);

        if ($response_mail_activate == true) {
          echo 'se envio el correo de activacion<br>';
        }else {
          echo 'no se pudo enviar el correo de activacion<br>';
        }
      }
    }

  }
}


function model_user_get_user_inactive($tbl, $prefix, $fields, $time) {

  $sql = 'SELECT ' . $fields . ' FROM    tbl_' . $tbl . '_info
          WHERE active_' . $prefix . ' = 0 AND 
          created_' . $prefix . ' <= NOW() - INTERVAL ' . $time . ';';

  $result = functions_sql_execute_query($sql);
  if (!empty($result)) {
    $rows = array();
    while($item = functions_sql_execute_get_dates($result)) {
      $rows[] = $item;
    }
    functions_sql_close_query_and_connection($result);
    return $rows;
  }
  functions_sql_close_query_and_connection($result);
  return FALSE;
}


function model_user_update_token_active_user($tbl, $prefix, $id_user, $token_active_user) {
  
  $sql = 'UPDATE tbl_' . $tbl . '_info SET token_active_account_' . $prefix . ' = "'.  $token_active_user . '"
   WHERE id_' . $prefix . ' = ' . $id_user . ';';
  $response = functions_sql_execute_query($sql);
  functions_sql_close_connection();

  return $response;
}

/*
 * Enviar correo a usuario registrado para activar la cuenta
 */
function control_utilities_send_mail_user_register($type, $token_active_user, $name_user, $email, $pass = FALSE) {

  $html = '';
  $html .='

  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
      <meta charset="utf-8"> <!-- utf-8 works for most cases -->
      <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldnt be necessary -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
      <title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->

      <style>

          /* What it does: Remove spaces around the email design added by some email clients. */
          /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
          html,
          body {
              margin: 0 auto !important;
              padding: 0 !important;
              height: 100% !important;
              width: 100% !important;
          }
          
          /* What it does: Stops email clients resizing small text. */
          * {
              -ms-text-size-adjust: 100%;
              -webkit-text-size-adjust: 100%;
          }
          
          /* What is does: Centers email on Android 4.4 */
          div[style*="margin: 16px 0"] {
              margin:0 !important;
          }
          
          /* What it does: Stops Outlook from adding extra spacing to tables. */
          table,
          td {
              mso-table-lspace: 0pt !important;
              mso-table-rspace: 0pt !important;
          }
                  
          /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
          table {
              border-spacing: 0 !important;
              border-collapse: collapse !important;
              table-layout: fixed !important;
              margin: 0 auto !important;
          }
          table table table {
              table-layout: auto; 
          }
          
          /* What it does: Uses a better rendering method when resizing images in IE. */
          img {
              -ms-interpolation-mode:bicubic;
          }
          
          /* What it does: A work-around for iOS meddling in triggered links. */
          .mobile-link--footer a,
          a[x-apple-data-detectors] {
              color:inherit !important;
              text-decoration: none !important;
          }
        
      </style>
      
      <!-- Progressive Enhancements -->
      <style>
          
          /* What it does: Hover styles for buttons */
          .button-td,
          .button-a {
              transition: all 100ms ease-in;
          }
          .button-td:hover,
          .button-a:hover {
              background: #555555 !important;
              border-color: #555555 !important;
          }


      </style>

  </head>
  <body width="100%" bgcolor="#ffffff" style="margin: 0; background: #ffffff;">
      <center style="width: 100%; background: #ffffff;">

          <!-- Visually Hidden Preheader Text : BEGIN -->
          <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
              Hola, has solicitado la inscripción en Mi HV. Actíva tu cuenta para acceder a nuestro portal de ofertas.
          </div>
          <!-- Visually Hidden Preheader Text : END -->

          <!--    
              Set the email width. Defined in two places:
              1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 600px.
              2. MSO tags for Desktop Windows Outlook enforce a 600px width.
          -->
          <div style="max-width: 600px; margin: auto;">
              <!--[if mso]>
              <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="600" align="center">
              <tr>
              <td>
              <![endif]-->

              <!-- Email Header : BEGIN -->
              <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">
                  <tr>
                      <td style="padding: 20px 0; text-align: center;">
                        <img alt="Saludo email" src="https://imagenes.mihv.com.co/email/saludo.jpg" alt="alt_text" border="0" align="center" style="width: 100%; max-width: 600px; background: none; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding-bottom:10px;">

                          <img alt="Header mi hv" src="https://imagenes.mihv.com.co/email/header-logo.jpg" width="90%" height="50" alt="alt_text" border="0" style="background: none; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                      </td>
                  </tr>
              </table>
              <!-- Email Header : END -->
              
              <!-- Email Body : BEGIN -->
              <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">
                  
                  

                  <!-- 1 Column Text + Button : BEGIN -->
                  <tr>
                      <td bgcolor="#ffffff">
                          <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                              <tr>
                                  <td style="padding: 40px; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                      Hola ' . $name_user . ' como estas?:
                                      <br><br>
                                      Has tomado una excelente decisión al inscribirte en nuestro portal y por ello te damos la bienvenida.
                                      <br><br>
                                      Queremos decirte que ya te encuentras registrado y te queda un paso el cual es confirmar éste correo por cuestiones de seguridad, pero es fácil, dale click en el botón Confirmar Mi Correo
                                      <br><br>
                                      <!-- Button : Begin -->
                                      <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" style="margin: auto;">
                                          <tr>
                                              <td style="border-radius: 3px; background: #222222; text-align: center;" class="button-td">
                                                  <a href="' . DOMAIN . '/controls/control_active_user.php?active_user=' . $token_active_user . '&type=' . $type . '" style="background: #337AB7; border: 15px solid #337AB7; font-family: sans-serif; font-size: 13px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold;" class="button-a" target="_blank">
                                                      &nbsp;&nbsp;&nbsp;&nbsp;<span style="color:#ffffff">Confirmar Mi Correo</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                                  </a>
                                              </td>
                                          </tr>
                                      </table>
                                      <!-- Button : END -->
                                      <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" style="margin: auto;" >
                                      <br>                                    
                                      Si presionaste el botón y aún no pasa nada, no hay lio; dale click en el siguiente link y si no funciona, cópialo:<br><br>
                                      <a href="' . DOMAIN . '/controls/control_active_user.php?active_user=' . $token_active_user . '&type=' . $type . '" style="word-wrap: break-word;">
                                      ' . DOMAIN . '/controls/control_active_user.php?active_user=' . $token_active_user . '&type=' . $type . '"
                                      </a>
                                      </table>
                                      <br><br>
                                      Una vez copiado abre una nueva pestaña o ventana en tu navegador, pégalo ahí y presiona ENTER y así confirmas tu correo.
                                      <br><br>
                                      Una vez hecho lo del link te invitamos a que ingreses a <a href="https://www.mihv.com.co">www.mihv.com.co</a> con tus datos y realices tu búsqueda.
                                      <br><br>';
                                      if ($pass != FALSE) {
                                        $html .='Estos son tus datos de acceso:<br>
                                        <b>Usuario: </b> ' . $email . '<br>
                                        <b>Clave: </b> ' . $pass . '';
                                      }else {
                                        $html .='Este es tu usuario de acceso:<br>
                                        <b>Usuario: </b> ' . $email . '<br>';
                                      }

                                      $html .='<br><br>
                                      Éxitos en tu búsqueda y felíz resto de día.
                                      <br><br><br>
                                      El equipo de Mi HV.
                                      <br><br><br>
                                      PD: Posiblemente te interese...<br>
                                  </td>
                                  </tr>
                          </table>
                      </td>
                  </tr>
              </table>
              <!-- Email Body : END -->

              <!-- 2 Even Columns : BEGIN -->
                  <tr>                    
                      <td bgcolor="#ffffff" align="center" height="100%" valign="top" width="100%" style="padding-bottom: 40px">
                          <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width:560px;">
                              <tr>
                                  <td align="center" valign="top" width="50%">
                                      <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                          <tr>
                                              <td style="text-align: center; padding: 0 10px;">
                                                  <a href="https:/www.google.com.co" target="_blank">
                                                  <img alt="pauta email" src="https://imagenes.mihv.com.co/email/200-200.png" width="200" height="200" alt="alt_text" class="center-on-narrow" style="border: 0;width: 100%;max-width: 200px; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                                  </a>
                                              </td>
                                          </tr>                                        
                                      </table>
                                  </td>
                                  <td align="center" valign="top" width="50%">
                                      <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                          <tr>
                                              <td style="text-align: center; padding: 0 10px;">
                                                  <a href="https:/www.google.com.co" target="_blank">
                                                  <img alt="pauta email" src="https://imagenes.mihv.com.co/email/200-200.png" width="200" height="200" alt="alt_text" class="center-on-narrow" style="border: 0;width: 100%;max-width: 200px; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                                  </a>
                                              </td>
                                          </tr>
                                          
                                      </table>
                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  <!-- Two Even Columns : END -->
            
              <!-- Email Footer : BEGIN -->
              <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;">
                  <tr>
                      <td style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center; color: #888888;">                        
                          <span class="mobile-link--footer"><a href="https://www.ingcoder.com" target="_blank">Mi HV (IngCoder SAS)</a></span><br><span class="mobile-link--footer">Carrera 13 # 13 - 24 Oficina 725<br>Bogotá D.C., C/marca, 110311 CO</span><br><span class="mobile-link--footer"><a href="tel:+573165269362"><img alt="whatsapp" src="https://imagenes.mihv.com.co/email/whatsapp.jpg" width="100%" style="max-width:9px;"> (+57) 316 526 9362</a></span>
                          <!--<br><br>
                          <unsubscribe style="color:#888888; text-decoration:underline;">unsubscribe</unsubscribe>-->
                      </td>
                  </tr>
              </table>
              <!-- Email Footer : END -->

              <!--[if mso]>
              </td>
              </tr>
              </table>
              <![endif]-->
          </div>
      </center>
  </body>
  </html>
  ';

  $subject = 'Activa tu usario MI HV';

  //incluir libreria para correo
  $base = dirname(dirname(__FILE__));
  include_once($base . '/cron/PHPMailer-master/class.phpmailer.php');
  include_once($base . '/cron/PHPMailer-master/class.smtp.php');

  $mail = new PHPMailer;
  $mail->IsSMTP();
  //permite modo debug para ver mensajes de las cosas que van ocurriendo
  //$mail->SMTPDebug = 2;
  $mail->SMTPAuth = true;
  $mail->SMTPSecure = "starttls";
  $mail->Host = "mail.mihv.co";
  $mail->Port = 587;
  $mail->Username = "informacion";
  $mail->Password = "Info2016*..*";
  $mail->From = "informacion@envios.mihv.co";    
  $mail->FromName = "MI HV - Activa tu usuario";
  $mail->Subject = $subject;
  $mail->AddAddress($email);
  $mail->MsgHTML(utf8_decode($html));
  $mail->IsHTML(true);
  //$mail->AddAttachment('../uploadAlDia/mi_archivo.jpg');
  
  if ($mail->Send()) {
    return true;
  }else {
    return false;
  }
}

